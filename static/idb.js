

function createIndexedDB() {
  if (!("indexedDB" in window)) {
    console.log("this browser supports indexedBD");
  }
  var version = 1;
  var db = "lepape"; // database
  var store = "cart"; // store

  // open the database
  var request = indexedDB.open(db, version); // connect + open database
  var db = false;

  // if error
  request.onerror = function(event) {
    console.log(event.target);
    console.trace();
  };

  // if success
  request.onsuccess = function(event) {
    console.log(event.target);
    console.trace();
  };

  // if onupgradeneeded
  request.onupgradeneeded = function(event) {
    console.log(event.target);
    console.trace();
    db = event.target.result;
    var objectStore = db.createObjectStore(store, { keyPath: "url" });
  };
}

const dbPromise = createIndexedDB();

function saveEventDataLocally(data) {
  let db;
  let request = indexedDB.open("lepape", 1);
  request.onsuccess = function(evt) {
    db = request.result;
    const tx = db.transaction("cart", "readwrite");
    data.forEach(value => {
      let request = tx.objectStore("cart").add(value);
    });

    return console.log(tx.complete);
  };
}

function getLocalEventData() {
  let db;
  let request = indexedDB.open("lepape", 1);
  request.onsuccess = function(evt) {
    db = request.result;
    const tx = db.transaction("cart", "readonly");
    const store = tx.objectStore("cart");
    return console.log(store.getAll());
  };
}

//const dataAdd = saveEventDataLocally(items);
//const dataRead = getLocalEventData();

export default {getLocalEventData,saveEventDataLocally}