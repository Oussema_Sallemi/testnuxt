importScripts(
  "https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js"
);

let items = [
  {
    _id: 123,
    name: "sandwich",
    price: 4.99,
    description: "A very tasty sandwich",
    created: new Date().getTime()
  },
  {
    _id: 124,
    name: "sandwich",
    price: 4.99,
    description: "A very tasty sandwich",
    created: new Date().getTime()
  }
];

const cacheName = "lepape-cache";

function createIndexedDB() {
  var version = 1;
  var db = "lepape"; // database
  var store = "cart"; // store

  // open the database
  var request = indexedDB.open(db, version); // connect + open database
  var db = false;
  // if error
  request.onerror = function(event) {
    console.log(event.target);
    console.trace();
  };

  // if success
  request.onsuccess = function(event) {
    console.log(event.target);
    console.trace();
  };

  // if onupgradeneeded
  request.onupgradeneeded = function(event) {
    console.log(event.target);
    console.trace();
    db = event.target.result;
    var objectStore = db.createObjectStore(store, {
      keyPath: "_id",
      autoIncrement: true
    });
  };
}

const dbPromise = createIndexedDB();

function saveEventDataLocally(apiInfo) {
  let db;
  let request = indexedDB.open("lepape", 1);
  request.onsuccess = function(evt) {
    db = request.result;
    const tx = db.transaction("cart", "readwrite");
    apiInfo.forEach(value => {
      let request = tx.objectStore("cart").add(value);
    });

    return tx.complete;
  };
}

function getLocalEventData() {
  let db;
  let request = indexedDB.open("lepape", 1);
  request.onsuccess = function(evt) {
    db = request.result;
    const tx = db.transaction("cart", "readonly");
    const store = tx.objectStore("cart");
    return console.log(store.getAll());
  };
}

workbox.setConfig({
  debug: true
});

workbox.precaching.cleanupOutdatedCaches();

workbox.precaching.precacheAndRoute([{"revision":"36c0e8bcea4c4f820605aad0ad74342e","url":"all.js"},{"revision":"ddf2e14a825fc1fff4b1d5f0cb48acda","url":"favicon.ico"},{"revision":"93ddb3a661a420cee1245637eb38bd8a","url":"fetch.js"},{"revision":"719a7348697feb29b778056388e34e63","url":"icon.ico"},{"revision":"74e8b3b16c9c3002c851fa9ba4dbda31","url":"icon.png"},{"revision":"90b0f6e53b9d9507d6ae5c0a2c199813","url":"idb.js"},{"revision":"74ce45e4abfa9c67b0be4602963d2c61","url":"iocn.jpg"},{"revision":"5034cfd9edf3fa7d7d23589ecb2488f9","url":"README.md"},{"revision":"83dd50aa7e60ecc51768cd73dab738ed","url":"register_service_worker.js"},{"revision":"5d221438dc636eabc031b9152e2e5df2","url":"workbox-7409304f.js"}]);

workbox.core.clientsClaim();
workbox.core.skipWaiting();

workbox.routing.registerRoute(
  new RegExp("/_nuxt/.*"),
  new workbox.strategies.CacheFirst({}),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("/.*"),
  new workbox.strategies.NetworkFirst({}),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("/"),
  new workbox.strategies.StaleWhileRevalidate({}),
  "GET"
);

workbox.routing.registerRoute(
  /.*\.(?:png|jpg|jpeg|svg|gif|webp|bmp|svg)/,
  new workbox.strategies.CacheFirst({
    cacheName: "api-images",
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  }),
  "GET"
);
workbox.routing.registerRoute(
  new RegExp("http://localhost:5000/image/.*"),
  new workbox.strategies.CacheFirst({}),
  "GET"
);

self.addEventListener("message", e => {
  if (e.data) {
    saveEventDataLocally(Object.values(e.data));
  }
});

const cachedFetch = request =>
  request.method != "GET"
    ? fetch(request)
    : caches.open(cacheName).then(cache =>
        cache.match(request).then(resp => {
          if (!!resp) {
            console.log("> from cache", request.url);
            return resp;
          } else {
            console.log("! not in cache", request.url);
            return fetch(request).then(response => {
              cache.put(request, response.clone());
              return response;
            });
          }
        })
      );

self.addEventListener("fetch", event =>
  event.respondWith(cachedFetch(event.request))
);

//const dataAdd = saveEventDataLocally(payload);
//const dataRead = getLocalEventData();
